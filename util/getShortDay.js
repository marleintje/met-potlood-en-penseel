/* eslint-disable sort-keys */
const getShortDay = (day) => {
	const shortDays = {
		monday: 'Mo',
		maandag: 'Ma',
		tuesday: 'Tu',
		dinsdag: 'Di',
		wednesday: 'We',
		woensdag: 'Wo',
		thursday: 'Th',
		donderdag: 'Do',
		friday: 'Fr',
		vrijdag: 'Vr',
		saturday: 'Sa',
		zaterdag: 'Za',
		sunday: 'Su',
		zondag: 'Zo',
	}

	return shortDays[day.toLowerCase()]
}

export default getShortDay
