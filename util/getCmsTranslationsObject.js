const getCmsTranslationsInObject = (english, dutch) => {
	const englishArray = english[Object.keys(english)[0]]
	const dutchArray = dutch[Object.keys(english)[0]]
	const objectWithTranslations = {}
	objectWithTranslations.en = englishArray
	objectWithTranslations.nl = dutchArray
	return objectWithTranslations
}

export default getCmsTranslationsInObject
