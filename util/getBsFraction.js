const getBsFraction = (hygraphEnumValue, invert = false) => {
	switch (hygraphEnumValue) {
		case 'Kwart':
			return !invert ? 3 : 9;
		case 'Derde':
			return !invert ? 4 : 8;
		case 'VijfTwaalfde':
			return !invert ? 5 : 7;
		case 'Helft':
			return 6;
		case 'ZevenTwaalfde':
			return !invert ? 7 : 5;
		case 'TweeDerde':
			return !invert ? 8 : 4;
		case 'DrieKwart':
			return !invert ? 9 : 3;
		default:
			return 6;
	}
}
export default getBsFraction
