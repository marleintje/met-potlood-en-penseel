const prependSlugWithLocale = (locale, slug) => `/${locale}/${slug}`

export default prependSlugWithLocale
