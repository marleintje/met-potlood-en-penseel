const getWrittenOutDate = (date, locale = 'nl', isShort = false) => {
	const dateObject = new Date(date)
	const day = dateObject.getDate()
	const month = dateObject.getMonth()
	const year = dateObject.getFullYear()
	const writtenOutMonths = {}
	writtenOutMonths['en-short'] = 'Jan. Feb. Mar. Apr. May Jun. Jul. Aug. Sep. Oct. Nov. Dec.'.split(' ')
	writtenOutMonths['nl-short'] = 'jan. feb. mrt apr. mei juni juli aug. sep. okt. nov. dec.'.split(' ')
	writtenOutMonths['en-long'] = 'January February March April May June July August September October November December'.split(' ')
	writtenOutMonths['nl-long'] = 'januari februari maart april mei juni juli augustus september oktober november december'.split(' ')

	const writtenOutMonth = writtenOutMonths[`${locale}-${isShort ? 'short' : 'long'}`][month]

	return `${day} ${writtenOutMonth} ${year}`
}

export default getWrittenOutDate
