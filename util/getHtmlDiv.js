import React from 'react'

import createMarkup from './createMarkup'

const getHtmlDiv = (html) => {
	if (!html) return null
	const htmlWithBr = html ? (html.split('\n')).join('<br>') : ''
	return <div dangerouslySetInnerHTML={createMarkup(htmlWithBr)} />
}

export default getHtmlDiv
