const getLocalizedArray = (localizedQueryResults) => {
	const localizedArray = []
	localizedQueryResults.forEach(localizedQueryResult => {
		const localizationOne = localizedQueryResult.localizations[0]
		const localizationTwo = localizedQueryResult.localizations[1]
		if (localizationOne) localizedArray.push(localizationOne)
		if (localizationTwo) localizedArray.push(localizationTwo)
	})
	return localizedArray
}

export default getLocalizedArray
