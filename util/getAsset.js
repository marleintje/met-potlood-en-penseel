const getAsset = (assets, id) => assets.find(asset => asset.id === id) || {}

export default getAsset
