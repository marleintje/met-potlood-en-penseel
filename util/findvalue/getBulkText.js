import getHtmlDiv from "../getHtmlDiv"

const getBulkText = (bulkTexts, uniqueKey) => getHtmlDiv(bulkTexts.find(({key}) => key === uniqueKey)?.text?.html)

export default getBulkText
