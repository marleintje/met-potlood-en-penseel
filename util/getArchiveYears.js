const getArchiveYears = () => {
	const years = []

	let i
	for (i = (new Date().getFullYear() - 1); i >= 2013; i--) {
		years.push(i)
	}

	return years
}

export default getArchiveYears
