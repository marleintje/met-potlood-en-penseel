const getYearlyExhibitionYears = () => {
	const years = []

	let i
	for (i = (new Date().getFullYear()); i >= 2020; i--) {
		years.push(i)
	}

	return years
}

export default getYearlyExhibitionYears
