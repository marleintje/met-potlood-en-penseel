/* eslint-disable sort-keys */
const getItemsPerDay = (itemsArray) => {
	const yearlyItemsPerDay = {
		monday: [],
		tuesday: [],
		wednesday: [],
		thursday: [],
		friday: [],
		saturday: [],
		sunday: [],
	}

	if (itemsArray && itemsArray.length) {
		itemsArray.forEach(item => {
			switch (item.day) {
				case 'Maandag':
				case 'Monday':
					yearlyItemsPerDay.monday.push(item)
					break
				case 'Dinsdag':
				case 'Tuesday':
					yearlyItemsPerDay.tuesday.push(item)
					break
				case 'Woensdag':
				case 'Wednesday':
					yearlyItemsPerDay.wednesday.push(item)
					break
				case 'Donderdag':
				case 'Thursday':
					yearlyItemsPerDay.thursday.push(item)
					break
				case 'Vrijdag':
				case 'Friday':
					yearlyItemsPerDay.friday.push(item)
					break
				case 'Zaterdag':
				case 'Saturday':
					yearlyItemsPerDay.saturday.push(item)
					break
				case 'Zondag':
				case 'Sunday':
					yearlyItemsPerDay.sunday.push(item)
					break
				default:
					break
			}
		})
	}

	return yearlyItemsPerDay
}

export default getItemsPerDay
