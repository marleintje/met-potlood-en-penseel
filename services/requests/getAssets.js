import { getAssetsQuery } from '../graphql-queries/asset'
import hygraphClient from '../../clients/hygraphClient'

const getAssets = async () => {
	const fetchedAssets = await hygraphClient.request(getAssetsQuery())
	const { assets } = fetchedAssets
	return assets
}

export default getAssets
