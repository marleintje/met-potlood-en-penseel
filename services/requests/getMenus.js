import hygraphClient from '../../clients/hygraphClient'
import getMenusQuery from '../graphql-queries/menus'

const getMenus = async (locale) => {
	const { menus } = await hygraphClient.request(getMenusQuery(locale))
	return menus
}

export default getMenus
