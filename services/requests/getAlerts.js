import getAlertsQuery from '../graphql-queries/alert'
import hygraphClient from '../../clients/hygraphClient'

const getAlerts = async (locale) => {
	const  { alerts } = await hygraphClient.request(getAlertsQuery(locale))
	return alerts
}

export default getAlerts
