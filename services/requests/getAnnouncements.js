import getAnnouncementsQuery from '../graphql-queries/announcement'
import hygraphClient from '../../clients/hygraphClient'

const getAnnouncements = async (locale = 'nl', year = null) => {
	const fetchedAnnouncements = await hygraphClient.request(getAnnouncementsQuery(locale, year))
	const { announcements } = fetchedAnnouncements

	const addSortedDate = (announcements) => {
		const announcementsWithDate = announcements.map(announcement => {
			const sortedDate = announcement.date || announcement.createdAt
			// Strip images (that are always at the end) from HTML. They will later be inserted via text.raw property, see Announcement.js
			const strippedHtml = announcement.text.html.split('<img')[0]

			return {
				...announcement,
				sortedDate,
				strippedHtml,
			}
		})
		return announcementsWithDate
	}

	// Add sortedDate property (if Hygraph user has added a date, take that one, otherwise, the announcement creation date)
	const announcementsWithSortedDate = addSortedDate(announcements)

	// Sort announcement from most recent to oldest, based on sortedDate property
	const sortedAnnouncements = announcementsWithSortedDate.sort((a, b) => ((new Date(a.sortedDate) < new Date(b.sortedDate)) ? 1 : -1))

	return sortedAnnouncements
}

export default getAnnouncements
