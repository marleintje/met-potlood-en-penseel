import hygraphClient from '../../clients/hygraphClient'
import getPageUrlsQuery from '../graphql-queries/pageUrls'

const getPageUrls = async () => {
	const { pages } = await hygraphClient.request(getPageUrlsQuery())
	return pages
}

export default getPageUrls
