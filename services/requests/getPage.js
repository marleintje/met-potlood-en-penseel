import hygraphClient from '../../clients/hygraphClient'
import getPagesQuery from '../graphql-queries/page'

const getPage = async (url, locale) => {
	const { pages } = await hygraphClient.request(getPagesQuery(url, locale))
	return pages?.[0]
}

export default getPage
