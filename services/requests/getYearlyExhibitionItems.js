import getYearlyExhibitionItemsQuery from '../graphql-queries/yearlyExhibitionItem'
import hygraphClient from '../../clients/hygraphClient'

const getYearlyExhibitionItems = async (locale = 'nl', year = null) => {
	const yearlyExhibitionItems = await hygraphClient.request(getYearlyExhibitionItemsQuery(locale, year))
	return yearlyExhibitionItems.yearlyExhibitionItems
}

export default getYearlyExhibitionItems
