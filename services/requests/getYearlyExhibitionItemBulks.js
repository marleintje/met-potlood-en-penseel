import getYearlyExhibitionBulkItemsQuery from '../graphql-queries/yearlyExhibitionItemBulk'
import hygraphClient from '../../clients/hygraphClient'

const getYearlyExhibitionItemsBulk = async (locale = 'nl', year = null) => {
	const items = await hygraphClient.request(getYearlyExhibitionBulkItemsQuery(locale, year))
	return items.yearlyExhibitionItemBulks
}

export default getYearlyExhibitionItemsBulk
