const getYearlyExhibitionBulkItemsQuery = (locale, year = null) => {
	const startYear = year === (new Date()).getFullYear() ? year - 1 : year
	return `{ yearlyExhibitionItemBulks(where: {year_gte: "${startYear}-01-01", year_lte: "${year}-12-31"}, locales: [${locale}]) {
			year
			title
			paintings(locales: nl) {
				handle
				url
				width
				height
				fileName
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
			}
		}
	}`
}

export default getYearlyExhibitionBulkItemsQuery
