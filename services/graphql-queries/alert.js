const getAlertsQuery = (locale) => `{
	alerts(locales: [${locale}]) {
		bootstrapColor
		endDate
		id
	  	text {
			html
		}
	}
}`

export default getAlertsQuery
