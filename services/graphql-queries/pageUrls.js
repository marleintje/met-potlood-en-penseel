const getPageUrlsQuery = () => `{
	pages {
		__typename
		localizations(includeCurrent: true) {
		  locale
		  url
		}
	}
}`

export default getPageUrlsQuery
