const getMenusQuery = (locale) => `{
	menus(locales: [${locale}, nl]){
		key
		menuHeadings {
			...on MenuHeading {
				id
				title
				slug
				subHeaders {
					...on MenuHeading {
						id
						title
						slug
					}
				}
			}
		}
	}
}`

export default getMenusQuery