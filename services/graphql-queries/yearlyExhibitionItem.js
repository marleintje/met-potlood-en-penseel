const getYearlyExhibitionItemsQuery = (locale, year = null) => {
	const startYear = year === (new Date()).getFullYear() ? year - 1 : year
	return `{
		${year
		? `yearlyExhibitionItems(first: 500, where: {year_gte: "${startYear}-01-01", year_lte: "${year}-12-31"}, locales: [${locale}]) {`
		: `yearlyExhibitionItems(first: 500, locales: [${locale}]) {`
}
			day
			format
			details
			id
			title
			artist
			year
			painting(locales: nl) {
				handle
				url
				width
				height
				fileName
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
			}
		}
	}`
}

export default getYearlyExhibitionItemsQuery
