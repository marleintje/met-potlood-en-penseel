const getAnnouncementsQuery = (locale, year = null) => {
	const startYear = year === (new Date()).getFullYear() ? year - 1 : year
	return `{
		${year
		? `announcements( where: {date_gte: "${startYear}-01-01", date_lte: "${year}-12-31"}, locales: [${locale}]) {`
		: `announcements( locales: [${locale}]) {`
}
			id
			title
			date
			createdAt
			text {
				raw
				html
			}
		}
	}`
}

export default getAnnouncementsQuery
