const getPageQuery = (url, locale = 'nl') => `{
	pages(where: { url: "${url}" }, locales: [${locale}, nl]) {
		id
		title
		isEditable
		seoTitle
		seoDescription
		url
		components {
		  ...on Spacer {
			__typename
			id
			mobile
			desktop
		  }
		  ...on TextImage {
			__typename
			id
			text {
			  html
			}
			textFraction
			isImageLeft
			image {
			  	url
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
				handle
				fileName
				width
				height
			}
		  }
		  ...on TextText {
			__typename
			id
			textLeft {
			  html
			}
			textRight {
				html
			  }
			fractionTextLeft
		  }
		  ...on Text {
			__typename
			id
			text {
			  html
			}
		  }
		  ...on HomepageBanner {
			__typename
			id
			imageTablet {
		      	url
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
				handle
				fileName
				width
				height
			}
			imageMobile {
				url
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
				handle
				fileName
				width
				height
			}
			imageDesktop {
			  	url
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
				handle
				fileName
				width
				height
			}
			titleColor {
			  hex
			}
		  }
		  ...on LatestNewsSummary {
			__typename
			id
			isVideoLeft
			videoCaption
			video {
				url
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
				handle
				fileName
				width
				height
			}
		  }
		  ...on Title {
			__typename
			id
			title
			depth
		  }
		  ...on InformationCards {
			__typename
			id
			cards {
			  id
			  image {
				url
				prefetchUrl: url (
					transformation: {
						image: { resize: { width: 10, fit: crop } }
					}
				)
				handle
				fileName
				width
				height
			  }
			  text {
				html
			  }
			}
		  }
		}
	  }
}`

export default getPageQuery
