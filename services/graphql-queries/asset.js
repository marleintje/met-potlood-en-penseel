export const getAssetsQuery = () => `{
	assets(first: 9001, where: {isOnMainWebsite: true}) {
		fileName
		height	
		id
		url
		prefetchUrl: url (
			transformation: {
				image: { resize: { width: 10, fit: crop } }
			}
		)
		width
	}
}`

export default {
	getAssetsQuery,
}
