const getBulkTextsQuery = (locale) => `{
	bulkTexts(locales: [${locale}, nl]) {
		key
		text {
			html
		}
	  
	}
}`
export default getBulkTextsQuery
