const getWordsQuery = (locale = 'nl') => `{
	words( locales: [${locale}, nl], orderBy: key_ASC) {
		key
		value
	}
}`

export default getWordsQuery
