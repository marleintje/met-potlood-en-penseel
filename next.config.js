const { i18n } = require('./next-i18next.config')

module.exports = {
	experimental: {
		// Temporary solution to avoid build error. TODO: Implement request throttling. Maybe with react query??
		// This is experimental but can be enabled to allow parallel threads with nextjs automatic static generation
		workerThreads: false,
		cpus: 1
	},
	i18n,
	images: {
		domains: ['media.graphcms.com', 'media.graphassets.com'],
	},
}
