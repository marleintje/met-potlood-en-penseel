module.exports = {
	defaultValue: '',
	input: [
		'components/**',
		'elements/**',
		'pages/**',
		// 'util/**',
		// 'pages/**/*.{js,jsx}' --> if only js and jsx
		// Use ! to filter out files or directories
		'!**/node_modules/**',
	],
	lexers: {
		js: ['JsxLexer'], // if you're writing jsx inside .js files, change this to JsxLexer
		jsx: ['JsxLexer'],
	},

	locales: ['en'],
	output: './data/words.json',
	// Supports $LOCALE and $NAMESPACE injection
	// Supports JSON (.json) and YAML (.yml) file formats
	// Where to write the locale files relative to process.cwd()

	sort: true,
}
