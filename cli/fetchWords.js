/* eslint-disable no-console */
import fs from 'fs'

import getWordsQuery from '../services/graphql-queries/word'
import hygraphClient from '../clients/hygraphClient'

const writeWordsToTranslationFiles = () => {
	const languagesToHandle = ['en', 'nl']
	console.info('Creating i18n common.json files from Hygraph')

	languagesToHandle.forEach(async language => {
		const data = await hygraphClient.request(getWordsQuery(language))
		const dataRestructured = data.words.reduce((acc, { key, value }) => {
			acc[key] = value
			return acc
		}, {})
		const filePath = `./public/locales/${language}/common.json`
		fs.writeFileSync(filePath, JSON.stringify(dataRestructured, null, '\t'))
		console.info(`Handled language: ${language}`)
	})
}

writeWordsToTranslationFiles()
