// More info at: https://dev.to/vuongddang/how-to-use-fontawesome-in-next-js-5bl5
// Find your icon on https://fontawesome.com/icons?d=gallery and add it below to use it in the codebase

import { library } from '@fortawesome/fontawesome-svg-core'

import {
	faBars,
	faFilePdf,
	faPaintBrush,
	faPalette,
	faSadCry,
	faSort,
	faThumbtack,
	faTimes,
} from '@fortawesome/free-solid-svg-icons'

export const fontAwesomeIconsLibrary = library.add(
	faBars,
	faFilePdf,
	faPaintBrush,
	faPalette,
	faSadCry,
	faSort,
	faThumbtack,
	faTimes,
)

export default fontAwesomeIconsLibrary
