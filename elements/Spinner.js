import { Spinner as BootstrapSpinner } from 'react-bootstrap'
import React from 'react'

const Spinner = ({ variant = 'primary' }) =><BootstrapSpinner animation='border' role='status' variant={variant}>
	<span className='sr-only'>Loading...</span>
</BootstrapSpinner>
	
export default Spinner
