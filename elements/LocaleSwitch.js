import Image from 'next/image'
import React from 'react'
import { useRouter } from 'next/router'

export default function LocaleSwitch({ pageUrls, setMenuIsOpen }) {
	const route = useRouter()
	const { section, year } = route.query
	const {locale }	= route

	const allPageUrls = pageUrls.map(({ localizations }) => localizations)
	const pageLocalization = allPageUrls.find(page => page.find(({ url }) => url === section))
	const counterLocalization = pageLocalization?.find(({ locale: pageLocale }) => locale !== pageLocale)

	const switchLocale = () => {
		setMenuIsOpen(false)
		if (year) {
			if (locale === 'nl' && section === 'archief') {
				route.push(`/archive/${year}`, `/archive/${year}`, { locale: 'en' })
			} else if (locale === 'en' && section === 'archive') {
				route.push(`/archief/${year}`, `/archief/${year}`, { locale: 'nl' })
			} else if (locale === 'nl' && section === 'jaarlijkse-tentoonstelling') {
				route.push(`/yearly-exhibition/${year}`, `/yearly-exhibition/${year}`, { locale: 'en' })
			} else if (locale === 'en' && section === 'yearly-exhibition') {
				route.push(`/jaarlijkse-tentoonstelling/${year}`, `/jaarlijkse-tentoonstelling/${year}`, { locale: 'nl' })
			}
		} else if(!section) {
			route.push('/', '/', { locale: locale === 'nl' ? 'en' : 'nl' })
		} else {
			route.push(counterLocalization.url, counterLocalization.url, { locale: locale === 'nl' ? 'en' : 'nl' })
		}
	}

	return <div className=''>
		<button
			aria-label='Switch language'
			className='btn btn-none border-none p-0 d-flex align-items-center'
			type='button'
			onClick={switchLocale}
		>
			<Image
				alt='Switch language'
				width={26}
				height={17}
				src={locale === 'nl'
					? '/flag-nl.svg'
					: '/flag-en.svg'
				}
			/>
			<span className='ms-3 h4 text-primary d-lg-none'>{locale === 'en' ? 'Nederlands' : 'Switch to English'}</span>
		</button>
	</div>
}

LocaleSwitch.defaultProps = {
	setMenuIsOpen: () => { },
}
