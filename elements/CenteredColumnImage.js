import Image from 'next/image'
import React from 'react'

import getSrc from '../util/getSrc'

export default function CenteredColumnImage({
	className, alt, src, url, width, height, prefetchUrl = '/favicon.png',
}) {
	return <div className='d-flex h-100 align-items-center'>
		<div className={`next-image-rounded-shadow mx-auto ${className}`}>
			<Image
				blurDataURL={prefetchUrl}
				placeholder={prefetchUrl ? 'blur' : null}
				alt={alt}
				src={getSrc(src ?? url)}
				width={width}
				height={height}
			/>
		</div>
	</div>
}

CenteredColumnImage.defaultProps = {
	alt: '',
	className: null,
}
