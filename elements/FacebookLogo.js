import React from 'react'

export default function FacebookLogo({
	dimension, color,
}) {
	return <div className='position-relative'>
		<img
			className='me-2'
			alt='Link to facebook page'
			height={dimension}
			src={`/facebook-logo-${color}.svg`}
			width={dimension}
		/>
		<a
			aria-label='Link to facebook page'
			href='https://www.facebook.com/Met-potlood-en-penseel-Eindhoven-330841160686040'
			className='stretched-link'
			rel='noreferrer'
			target='_blank'
		/>
	</div>
}

FacebookLogo.defaultProps = {
	color: 'white',
	dimension: '25px',
}
