import Link from 'next/link'
import React from 'react'
import { useRouter } from 'next/router'

export default function InternalLink({
	className,
	children,
	href,
	passHref,
	prefetch,
	stretched,
}) {
	const route = useRouter()
	const { locale } = route

	const usedLocale = locale || (route.asPath.indexOf('nl') > -1 ? 'nl' : 'en')

	return <Link prefetch={prefetch} locale={usedLocale} href={href} passHref={passHref}>
		{stretched
			? <a className={`stretched-link ${className}`} />
			: <a className={className}>{children}</a>}
	</Link>
}

InternalLink.defaultProps = {
	className: null,
	href: '/',
	passHref: true,
	prefetch: null,
}
