/* eslint-disable jsx-a11y/control-has-associated-label */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'

export default function BackDrop({
	isOpen, onClick,
}) {
	return <>
		{/* Backdrop black background */}
		<button
			aria-label='Backdrop'
			type='button'
			className={`position-fixed mobile-header-menu-bg ${isOpen}`}
			onClick={onClick}
		/>
		{/* Top-right closing-cross button */}
		<button
			aria-label='Close menu'
			type='button'
			className={`position-fixed btn mobile-header-menu-bg-cross ${isOpen}`}
			onClick={onClick}
		>
			<FontAwesomeIcon
				className='text-white text-size-1'
				icon='times'
			/>
		</button>
	</>
}

BackDrop.defaultProps = {
	isOpen: '',
	onClick: () => {},
}
