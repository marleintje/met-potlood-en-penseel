import Document, {
	Head, Html, Main, NextScript,
} from 'next/document'
import React from 'react'

class MyDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx)
		return { ...initialProps }
	}

	render() {
		const { locale } = this.props.__NEXT_DATA__.query
		return (
			<Html lang={locale || 'nl'}>
				<Head>
					{/* / Global site tag (gtag.js) - Google Analytics */}
					<script async src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GA_TRACKING_ID}`} />
					<script
						dangerouslySetInnerHTML={{
						__html: `
							window.dataLayer = window.dataLayer || [];
							function gtag(){dataLayer.push(arguments);}
							gtag('js', new Date());

							gtag('config', '${process.env.GA_TRACKING_ID}', {
								'anonymize_ip': true,
								'page_path': window.location.pathname
							});
						`,
						}}
					/>
				</Head>
				<body>
					<Main />
					<NextScript />
					{/* Not sure if this preloading and preconnect thing works... */}
					<link rel='preload' href='/preloaded.css' as='style' />
					<link rel='stylesheet' href='/preloaded.css' />
				</body>
			</Html>
		)
	}
}

export default MyDocument
