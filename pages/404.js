import { Col, Container, Row } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import getAlerts from '../services/requests/getAlerts'
import getBulkText from '../util/findvalue/getBulkText'
import getBulkTextsQuery from '../services/graphql-queries/bulkText'
import hygraphClient from '../clients/hygraphClient'
import Wrapper from '../components/Wrapper'
import getMenus from '../services/requests/getMenus'
import getPageUrls from '../services/requests/getPageUrls'

export async function getStaticProps({ locale }) {
	const alerts = await getAlerts(locale)
	const menus = await getMenus(locale)
	const pageUrls = await getPageUrls()
	const bulkTextsData = await hygraphClient.request(getBulkTextsQuery(locale))
	const { bulkTexts } = bulkTextsData

	const today = new Date().toISOString()

	return {
		props: {
			...(await serverSideTranslations(locale, ['common'])),
			alerts,
			bulkTexts,
			menus,
			pageUrls,
			today,
		},
	}
}

export default function Custom404({
	alerts,
	bulkTexts,
	menus,
	pageUrls,
	today,
}) {
	const errorText = getBulkText(bulkTexts, 'text-404')
	return <Wrapper pageUrls={pageUrls} menus={menus} alerts={alerts} today={today}>
		<Container className='my-4 my-lg-5'>
			<Row>
				<Col lg={4} className='my-auto text-center'>
					<FontAwesomeIcon
						className='fa-6x text-primary'
						icon='sad-cry'
					/>
				</Col>
				<Col lg={8} className='my-auto'>
					<div className='text-center text-lg-left'>
						{errorText}
					</div>
				</Col>
			</Row>
		</Container>
	</Wrapper>
}
