import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useRouter } from 'next/router'

import getArchiveYears from '../../../util/getArchiveYears'
import getAlerts from '../../../services/requests/getAlerts'
import getAnnouncements from '../../../services/requests/getAnnouncements'
import getAssets from '../../../services/requests/getAssets'
import getYearlyExhibitionYears from '../../../util/getYearlyExhibitionYears'
import PageYearArchive from '../../../components/PageYearArchive'
import PageYearExhibition from '../../../components/PageYearExhibition'
import getYearlyExhibitionItems from '../../../services/requests/getYearlyExhibitionItems'
import getMenus from '../../../services/requests/getMenus'
import getPageUrls from '../../../services/requests/getPageUrls'
import getYearlyExhibitionItemsBulk from '../../../services/requests/getYearlyExhibitionItemBulks'

export async function getStaticProps({locale, params}) {
	const assets = await getAssets()
	const alerts = await getAlerts(locale)
	const announcements = await getAnnouncements(locale, params.year || null)
	const yearlyExhibitionItems = await getYearlyExhibitionItems(locale, params.year || null)
	const yearlyExhibitionItemsBulk = await getYearlyExhibitionItemsBulk(locale, params.year || null)
	const pageUrls = await getPageUrls()
	const menus = await getMenus(locale)

	const today = new Date().toISOString()

	return {
		props: {
			...(await serverSideTranslations(locale, ['common'])),
			alerts,
			announcements,
			assets,
			today,
			yearlyExhibitionItems,
			yearlyExhibitionItemsBulk,
			pageUrls,
			menus,
		},
	}
}

export async function getStaticPaths() {
	const years = getArchiveYears()
	const exhibitionYears = getYearlyExhibitionYears()

	const paths = []
	years.forEach(preMadeYear => {
		paths.push({ locale: 'en', params: { section: 'archive', year: `${preMadeYear}` } })
		paths.push({ locale: 'nl', params: { section: 'archief', year: `${preMadeYear}` } })
	})
	exhibitionYears.forEach(preMadeYear => {
		paths.push({ locale: 'en', params: { section: 'yearly-exhibition', year: `${preMadeYear}` } })
		paths.push({ locale: 'nl', params: { section: 'jaarlijkse-tentoonstelling', year: `${preMadeYear}` } })
	})
	return {
		fallback: false,
		paths,
	}
}

export default function Section({
	alerts,
	announcements,
	assets,
	today,
	yearlyExhibitionItems,
	yearlyExhibitionItemsBulk,
	pageUrls,
	menus
}) {
	const route = useRouter()
	const { section } = route.query
	if (section === 'archive' || section === 'archief') return <PageYearArchive
		alerts={alerts}
		assets={assets}
		announcements={announcements}
		today={today}
		pageUrls={pageUrls}
		menus={menus}
	/>
	if (section === 'yearly-exhibition' || section === 'jaarlijkse-tentoonstelling') return <PageYearExhibition
		alerts={alerts}
		assets={assets}
		today={today}
		yearlyExhibitionItems={yearlyExhibitionItems}
		yearlyExhibitionItemsBulk={yearlyExhibitionItemsBulk}
		pageUrls={pageUrls}
		menus={menus}
	/>

	return <div>Deze pagina doet het niet. Gelieve terug te keren naar de homepage.</div>
}
