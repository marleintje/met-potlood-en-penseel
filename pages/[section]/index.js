import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import Container from 'react-bootstrap/Container'
import CustomHead from '../../components/CustomHead'
import getAlerts from '../../services/requests/getAlerts'
import getAnnouncements from '../../services/requests/getAnnouncements'
import getAssets from '../../services/requests/getAssets'
import hygraphClient from '../../clients/hygraphClient'
import InternalLink from '../../elements/InternalLink'
import PageContact from '../../components/PageContact'
import PageUpdates from '../../components/PageUpdates'
import Wrapper from '../../components/Wrapper'
import getPageUrlsQuery from '../../services/graphql-queries/pageUrls'
import getPage from '../../services/requests/getPage'
import getBulkTextsQuery from '../../services/graphql-queries/bulkText'
import Page from '../../components/Page'
import getMenus from '../../services/requests/getMenus'
import getPageUrls from '../../services/requests/getPageUrls'

export async function getStaticProps({
	locale,
	params: { section }
}) {
	const alerts = await getAlerts(locale)
	const announcements = await getAnnouncements(locale, new Date().getFullYear())
	const assets = await getAssets()
	const menus = await getMenus(locale)
	const pageUrls = await getPageUrls()
	const page = await getPage(section, locale)
	// TODO Deze shit is echt lelijk met die bulkText. Please refactor/deprecate.
	const bulkTextsData = await hygraphClient.request(getBulkTextsQuery(locale))
	const { bulkTexts } = bulkTextsData

	const today = new Date().toISOString()

	return {
		props: {
			...(await serverSideTranslations(locale, ['common'])),
			alerts,
			announcements,
			assets,
			page,
			bulkTexts,
			today,
			menus,
			pageUrls
		},
	}
}

export async function getStaticPaths() {
	const { pages}  = await hygraphClient.request(getPageUrlsQuery())
	const paths = []

	pages.forEach(page => {
		const localizationOne = page.localizations[0]
		const localizationTwo = page.localizations[1]
		if (localizationOne) paths.push({ locale: localizationOne.locale, params: {
			section: localizationOne.url
		} })
		if (localizationTwo) paths.push({ locale: localizationTwo.locale, params: {
			section: localizationTwo.url
		} })
	})

	return {
		fallback: false,
		// Now contains all paths main pages for website (nl/over-ons, en/about-us, nl/actueel, en/updates, etc)
		paths,
	}
}

export default function Section({
	alerts,
	announcements,
	assets,
	page,
	bulkTexts,
	today,
	menus,
	pageUrls
}) {
	const { title, url, seoTitle, seoDescription } = page

	if (!!page?.isEditable) return <Page
		page={page}
		assets={assets}
		alerts={alerts}
		today={today}
		announcements={announcements}
		bulkTexts={bulkTexts}
		menus={menus}
		pageUrls={pageUrls}
	/>

	let pageContent
	switch (url) {
		case 'updates':
		case 'actueel':
			pageContent = <PageUpdates
				key={page.id}
				assets={assets}
				announcements={announcements}
				bulkTexts={bulkTexts}
				/>
				break
		case 'contact':
			pageContent = <PageContact
				key={page.id}
				bulkTexts={bulkTexts}
			/>
			break
		default:
			pageContent = <InternalLink key={page.id}>
				Back to homepage &gt;
			</InternalLink>
			break
	}

	return <Wrapper menus={menus} pageUrls={pageUrls} alerts={alerts} today={today}>
		<Container className='my-4 my-lg-5'>
			<h1>{title}</h1>
			<CustomHead
				pageUrls={pageUrls}
				assets={assets}
				description={seoDescription}
				title={seoTitle}
			/>
			{pageContent}
		</Container>
	</Wrapper>
}
