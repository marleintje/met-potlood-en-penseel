/* eslint-disable func-names */
/* eslint-disable no-console */

export default function (req, res) {
	require('dotenv').config()
	const {
		hierMoetHetHeen: email,
		zoHeetIk: userName,
		email: honeyPotEmail,
		message,
		subject,
		name: honeyPotName
	} = req.body

	if (!!honeyPotEmail || !!honeyPotName) {
		res.status(200).json(
			{
				message: 'Succes van API maar eigenlijk niks verzonden, want je bent een fraudeur die met verborgen velden kloot'
			})
		return undefined
	}

	// https:// medium.com/nerd-for-tech/coding-a-contact-form-with-next-js-and-nodemailer-d3a8dc6cd645#cfb1
	// eslint-disable-next-line global-require
	let nodemailer = require('nodemailer')

	const transporter = nodemailer.createTransport({
		port: 465,
		host: "mail.mijndomein.nl",
		auth: {
			user: 'noreply@potloodenpenseel.nl',
			pass: process.env.MIJNDOMEIN_EMAIL_PASSWORD,
		},
		secure: true,
	})

	const mailData = {
		from: 'noreply@potloodenpenseel.nl',
		to: 'info@potloodenpenseel.nl',
		replyTo: email,
		subject: `Bericht van ${userName}`,
		text: "Onderwerp: "
			+ subject
			+ " | Verzonden vanaf de website door: "
			+ userName
			+ ", email: "
			+ email
			+ ". | Bericht: "
			+ message,
		html: `
		<h1>Bericht via contactformulier website</h1>
		<p>
			<strong>Naam afzender: </strong>${userName}<br>
			<strong>E-mail afzender: </strong>${email}<br>
		</p>
		<h2>${subject}<br></h2>
		<p>${message}</p>
	  `
	}

	transporter.sendMail(mailData, function (err, info) {
		if (err) {
			res.status(500).json({ message: 'Error stauts 500 from pages/api/contact.js API call: ', err })
			console.log(err)
		}
		else {
			res.status(200).json({ message: 'Status success from pages/api/contact.js API call' })
			console.log(info)
		}
	})
}
