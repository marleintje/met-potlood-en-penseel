import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import getAlerts from '../services/requests/getAlerts'
import getAnnouncements from '../services/requests/getAnnouncements'
import getAssets from '../services/requests/getAssets'
import getPage from '../services/requests/getPage'
import getBulkTextsQuery from '../services/graphql-queries/bulkText'
import hygraphClient from '../clients/hygraphClient'
import Page from '../components/Page'
import getMenus from '../services/requests/getMenus'
import getPageUrls from '../services/requests/getPageUrls'

export async function getStaticProps({ locale }) {
	const page = await getPage('home', locale)
	const alerts = await getAlerts(locale)
	const announcements = await getAnnouncements(locale, new Date().getFullYear())
	const assets = await getAssets()
	const menus = await getMenus(locale)
	const pageUrls = await getPageUrls()

	const bulkTextsData = await hygraphClient.request(getBulkTextsQuery(locale))
	const { bulkTexts } = bulkTextsData
	const today = new Date().toISOString()

	return {
		props: {
			...(await serverSideTranslations(locale, ['common'])),
			alerts,
			announcements,
			assets,
			bulkTexts,
			today,
			page,
			menus,
			pageUrls,
		},
	}
}

const Home = ({
	page, assets, alerts, announcements, bulkTexts, today, menus, pageUrls
}) => <Page
	page={page}
	alerts={alerts}
	today={today}
	announcements={announcements}
	bulkTexts={bulkTexts}
	assets={assets}
	menus={menus}
	pageUrls={pageUrls}
/>

export default Home