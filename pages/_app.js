/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react'
import { appWithTranslation } from 'next-i18next'
import SSRProvider from 'react-bootstrap/SSRProvider'
import ZoomImageContext from '../components/ZoomImageContext'

import '../data/fontAwesomeIcons'
import '../styling/index.scss'

const App = ({ Component, pageProps }) => {
	const [zoomedImage, setZoomedImage] = useState('')

	const updateZoomedImage = (id) => setZoomedImage(id)

	useEffect(() => {
		if ('serviceWorker' in navigator) {
			window.addEventListener('load', () => {
				navigator.serviceWorker.register('/service-worker.js', { scope: '.' }).then(
					(registration) => {
						console.warn('Service Worker registration successful with scope: ', registration.scope)
					},
					(err) => {
						console.warn('Service Worker registration failed: ', err)
					},
				)
			})
		}
	}, [])

	return <SSRProvider>
		<ZoomImageContext.Provider value={{ updateZoomedImage, zoomedImage }}>
			<Component {...pageProps} />
		</ZoomImageContext.Provider>
	</SSRProvider>
}

export default appWithTranslation(App)

// TODO (List of ideas in general)
// Use React Query + actual graphql extension files to optimze/speed up data fetching
// On build, fetch Hygraph words and write to common.json files.
