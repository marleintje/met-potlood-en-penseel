/* eslint quote-props: 'off' */

module.exports = {
	// https://github.com/airbnb/javascript/blob/master/packages/eslint-config-airbnb/rules/react.js
	env: {
		jest: true,
	},
	// TODO: I just grabbed some fixes from the internet to replace babel-eslint with @babel/eslint-parser
	// But next/babel gives warning during build. Double check.
	extends: ["next/babel","next/core-web-vitals", "airbnb"],
	globals: {
		'document': false,
		'fetch': false,
		'FormData': false,
		'localStorage': false,
		'shallow': false,
		'window': false,
	},
	parser: '@babel/eslint-parser',
	parserOptions: {
		requireConfigFile: false,
		babelOptions: {
			babelrc: false,
			configFile: false,
			// your babel options
			presets: ["@babel/preset-env"],
		},
	},
	root: true,
	rules: {
		'arrow-parens': 'off',
		'camelcase': 'off',
		'curly': 'off',
		'implicit-arrow-linebreak': 'off',
		'import/no-named-as-default': 'off',
		'indent': ['error', 'tab', {
			'ignoredNodes': ['JSXAttribute', 'JSXSpreadAttribute'],
			'SwitchCase': 1,
		}],
		'jsx-a11y/anchor-has-content': 'off',
		'jsx-a11y/anchor-is-valid': 'off',
		'jsx-a11y/label-has-associated-control': 'off',
		'jsx-a11y/label-has-for': 'off',
		'jsx-quotes': ['error', 'prefer-single'],
		'linebreak-style': 'off',
		'max-len': ['error', { code: 140, comments: 200 }],
		'no-case-declarations': 'off',
		'no-console': ['error', { allow: ['warn'] }],
		'no-floating-decimal': 'off',
		'no-nested-ternary': 'off',
		'no-plusplus': 'off',
		'no-return-assign': 'off',
		'no-shadow': 'off',
		'no-tabs': 'off',
		'no-underscore-dangle': 'off',
		'nonblock-statement-body-position': 'off',
		'prefer-destructuring': ['error', {
			'array': false,
			'object': true,
		}],
		'quotes': ['warn', 'single', { avoidEscape: true }],
		'react/jsx-closing-tag-location': 'off',
		'react/jsx-curly-newline': 'off',
		'react/jsx-filename-extension': 'off',
		'react/jsx-indent': [1, 'tab'],
		'react/jsx-indent-props': [1, 'tab'],
		'react/jsx-one-expression-per-line': 'off',
		'react/jsx-props-no-spreading': 'off',
		'react/jsx-wrap-multilines': 'off',
		'react/no-array-index-key': 'off',
		'react/no-danger': 'off',
		'react/prop-types': 'off',
		'semi': [2, 'never'],
		'sort-imports': ['error', {
			'allowSeparatedGroups': true,
			'ignoreCase': true,
		}],
		'sort-keys': ['error', 'asc', { 'caseSensitive': false, 'minKeys': 2, 'natural': false }],
	},
}
