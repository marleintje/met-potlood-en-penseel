# met Potlood en Penseel
## About this project

This repository houses the front-end for the website https://potloodenpenseel.nl (domain bought on mijndomein.nl). Original domain in Netlify was https://met-potlood-en-penseel.netlify.app/. It has replaced the old website https://potloodenpenseel.dse.nl/.

## Stack
- [GitLab](https://gitlab.com/). The platform where this private repository is hosted.
- [Netlify](https://www.netlify.com/). The platform where we build and deploy the website. The current pricing plan is **FREE**, with max 300 build minutes and 100GB Bandwidth per month.
- [Hygraph](https://hygraph.com/). A headless Content Management System where all content of the website is stored and managed. The developer uses Hygraph to create and define models (e.g. the "Alert" model contains required text field, optional endDate and bootstrapColor, etc.) and try out graphQL queries in the Hygraph playground to fetch the data. All API calls from this project are done to Hygraph to fetch the content. 99% of the content comes from Hygraph, barely any content is hardcoded in this project. The current pricing plan is **FREE**, with max 1.000.000 API operations and 100GB asset traffic per month, and max total 5000 records.
- [NextJS](https://nextjs.org/docs). This project is developed using NextJS. It is a [ReactJS](https://reactjs.org/) project, with the the additional benefit of server side rendering. This means that most HTML is already served from the server on client request (and not build up by the client based on JS file), which is better for crawlers and hence for SEO ranking (good findability on Google).
- [MijnDomein](https://www.mijndomein.nl/, https://mail.mijndomein.nl). Domain name and e-mail provider of https://potloodenpenseel.nl. This costs **€11 per year** for the domain name, and **€36 per year** for the e-mail address info@potloodenpenseel.nl. Domain owned by Marlein.

## Setup

### Software on your computer
- Install `Node` and `npm`. By installing npm, you should have node. https://www.npmjs.com/get-npm.
  Important note for Mac users (on 21/04/2021): Node 15 may give error along the line, so install Node 14 preferably.
- Use an advanced code editor, like `Visual Studio Code` https://code.visualstudio.com/

### Clone and run the repo
- Create a [GitLab](https://gitlab.com/) account.
- In your your Gitlab account, under `Preferences/SSH Keys`, add your computer's SSH key. (Find your SSH key by running cat ~/`.ssh/id_rsa.pub` or `cat ~/.ssh/id_dsa.pub` in your terminal)
- Ask Marlein for access to the Potlood&Penseel repository
- Run `git clone git@gitlab.com:marleintje/met-potlood-en-penseel.git` (= cloning via SSH)
- At project root (same level of this README), rename `.env.example` to `.env.local`. Fill in the correct values. If you don't know, ask Marlein or get access to Netlify (also via Marlein :P) and look them up in the pipeline.
- At project root, run `npm install`. (This installs all project dependent packages on your computer as specified in `package.json`)
- At project root, run `npm start`
- Open `http://localhost:3000/`. You should now see the website.
- To view the code itself, open it in your favorite editor (like with command `code .` in VS Code)

## Good to know
- To push new code, create branch, commits, push, pull request.
- Changes (so accepted PRs) to the `main` branch will automatically trigger a build in Netlify.
- This project uses the NextJS function [getStaticProps](https://nextjs.org/docs/basic-features/data-fetching#getstaticprops-static-generation). This means that NextJS will prerender this page at build time and do all API calls at build time as well. It results in fast pages once live! Locally however this function will just fetch the data when the page is requested, so each page load take a couple of seconds.
- If you wish to do a local build (cf point above), then run `npm run build`. Run `npm start-server` to check out your last local build on localhost:3000.

## Useful links:
- [Refresh your SASS knowledge](https://sass-lang.com/guide).
- [Refresh your bootstrap knowledge](https://getbootstrap.com/docs/5.2/getting-started/introduction/).
