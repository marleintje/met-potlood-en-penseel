import * as Yup from 'yup'
import {
	Alert, Button, Form,
} from 'react-bootstrap'
import React, { useState } from 'react'
import { Trans, useTranslation } from 'next-i18next'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

export default function ContactForm() {
	const { t } = useTranslation()

	// https://medium.com/nerd-for-tech/coding-a-contact-form-with-next-js-and-nodemailer-d3a8dc6cd645
	// https://jasonwatmore.com/post/2021/09/03/next-js-form-validation-example-with-react-hook-form
	const validationSchema = Yup.object().shape({
		hierMoetHetHeen: Yup.string()
			.required(t('form_validation_field_required', 'Dit veld is verplicht'))
			.email(t('form_validation_field_invalid', 'Gelieve een geldige waarde in te vullen')),
		message: Yup.string()
			.required(t('form_validation_field_required', 'Dit veld is verplicht'))
			.max(1200,
				t('form_validation_field_max', {
					count: 1200,
					defaultValue: 'Maximum {{count}} characters',
				})),
		subject: Yup.string()
			.required(t('form_validation_field_required', 'Dit veld is verplicht'))
			.max(60, t('form_validation_field_max', {
				count: 60,
				defaultValue: 'Maximum {{count}} characters',
			})),
		zoHeetIk: Yup.string()
			.required(t('form_validation_field_required', 'Dit veld is verplicht'))
			.max(60, t('form_validation_field_max', {
				count: 60,
				defaultValue: 'Maximum {{count}} characters',
			})),
	})

	const formOptions = { mode: 'onBlur', resolver: yupResolver(validationSchema) }

	const {
		register, handleSubmit, formState, reset,
	} = useForm(formOptions)
	const { errors } = formState

	const [contactFormIsSubmitted, setContactFormIsSubmitted] = useState(false)
	const [formResponseIsSuccess, setFormResponseIsSuccess] = useState(true)
	const [isSubmitting, setIsSubmitting] = useState(false)

	function onSubmit(data) {
		setIsSubmitting(true)
		fetch('/api/contact', {
			body: JSON.stringify(data),
			headers: {
				Accept: 'application/json, text/plain, */*',
				'Content-Type': 'application/json',
			},
			method: 'POST',
		}).then((res) => {
			setIsSubmitting(false)
			setContactFormIsSubmitted(true)
			if (res.status === 200) {
				// eslint-disable-next-line no-console
				console.log('Response succeeded!')
				reset()
			} else {
				console.warn('Response failed!')
				setFormResponseIsSuccess(false)
			}
		}).catch((e) => { console.warn('Error in submitting form server side', e) })

		return false
	}

	return contactFormIsSubmitted ? (
		formResponseIsSuccess ? <Alert variant='success'>
			<p className='mb-0'>
				<Trans i18nKey='contact_form_submit_success_message'>
					Bedankt voor je e-mail! We zullen je bericht verwerken.
				</Trans>
			</p>
		</Alert> : <Alert variant='danger'>
			<p className='mb-0'>
				<Trans i18nKey='contact_form_submit_failure_message'>
					Oh nee! Er is iets misgegaan bij het versturen van het formulier.
					Gelieve ons een belletje te geven of direct contact met ons op te nemen via info@potloodenpenseel.nl.
				</Trans></p>
		</Alert>
	) : (
		<Form onSubmit={handleSubmit(onSubmit)}>
			<label className='honing' htmlFor='name' />
			<input
				{...register('name')}
				className='honing'
				autoComplete='new-password'
				type='text'
				id='name'
				name='name'
				placeholder='Your name here'
			/>
			<label className='honing' htmlFor='email' />
			<input
				{...register('email')}
				className='honing'
				autoComplete='new-password'
				type='email'
				id='email'
				name='email'
				placeholder='Your e-mail here'
			/>
			<Form.Group className='mb-3 position-relative' controlId='form.hierMoetHetHeen'>
				<Form.Label>
					<Trans i18nKey='contact_form_field_email'>
						E-mail adres waarop we je kunnen bereiken
					</Trans>
				</Form.Label>
				<Form.Control
					{...register('hierMoetHetHeen')}
					autoComplete='new-password'
					className={errors.hierMoetHetHeen ? 'is-invalid' : ''}
					name='hierMoetHetHeen'
					type='email'
				/>
				<div className='invalid-feedback'>{errors.hierMoetHetHeen?.message}</div>
			</Form.Group>
			<Form.Group className='mb-3 position-relative' controlId='form.zoHeetIk'>
				<Form.Label>
					<Trans i18nKey='contact_form_field_name'>
						Je naam
					</Trans>
				</Form.Label>
				<Form.Control
					{...register('zoHeetIk')}
					autoComplete='new-password'
					className={errors.userName ? 'is-invalid' : ''}
					name='zoHeetIk'
					type='text'
				/>
				<div className='invalid-feedback'>{errors.userName?.message}</div>
			</Form.Group>
			<Form.Group className='mb-3 position-relative' controlId='form.subject'>
				<Form.Label>
					<Trans i18nKey='contact_form_field_subject'>
						Onderwerp
					</Trans>
				</Form.Label>
				<Form.Control
					{...register('subject')}
					className={errors.subject ? 'is-invalid' : ''}
					name='subject'
					type='text'
				/>
				<div className='invalid-feedback'>{errors.subject?.message}</div>
			</Form.Group>
			<Form.Group className='mb-3 position-relative' controlId='form.message'>
				<Form.Label>
					<Trans i18nKey='contact_form_field_message'>
						Bericht
					</Trans>
				</Form.Label>
				<Form.Control
					{...register('message')}
					className={errors.message ? 'is-invalid' : ''}
					name='message'
					as='textarea'
					rows='8'
				/>
				<div className='invalid-feedback'>{errors.message?.message}</div>
			</Form.Group>
			<Button
				className='mt-2'
				disabled={isSubmitting}
				variant='primary'
				type='submit'
			>
				<Trans i18nKey='contact_form_field_submit'>
					Versturen
				</Trans>
				{isSubmitting && <span className='spinner-border spinner-border-sm ms-1' />}
			</Button>
		</Form>
	)
}
