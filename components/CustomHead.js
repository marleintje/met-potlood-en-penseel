/* eslint-disable max-len */
import Head from 'next/head'
import React from 'react'
import { useRouter } from 'next/router'

import getAsset from '../util/getAsset'

export default function CustomHead({
	assets, description, title, pageUrls
}) {
	const route = useRouter()
	const { section } = route.query
	const { locale } = route
	const counterLocale = locale === 'nl' ? 'en' : 'nl'

	// TODO Potentially create helper of lines 19 to 21 because same lines (not DRY) in LocaleSwitch.js
	const allPageUrls = pageUrls.map(({ localizations }) => localizations)
	const pageLocalization = allPageUrls.find(page => page.find(({ url }) => url === section))
	const counterLocalization = pageLocalization?.find(({ locale }) => locale === counterLocale)
	const domainName = 'https://potloodenpenseel.nl'

	const currentHref = section ? `${domainName}/${locale}/${section}` : `${domainName}/${locale}`
	const counterLocaleSlug = counterLocalization?.url ?? ''
	const counterHref = section ? `${domainName}/${counterLocale}/${counterLocaleSlug}` : `${domainName}/${counterLocale}`

	// TODO Optional: add animation between page transitions

	const headTitle = title || (locale === 'en'
		? 'Drawing and painting association - Eindhoven | Met Potlood en Penseel.'
		: 'Teken en schildervereniging - Eindhoven | Met Potlood en Penseel.')

	const headDescription = description || (locale === 'en'
		? 'A drawing and painting association for people who want to paint together with others in a suitable workspace under professional guidance. View our studio!'
		: 'Een teken- en schildervereniging voor mensen die samen met anderen willen schilderen in een passende werkruimte onder professionele begeleiding. Bekijk ons atelier!')

	const ogImageWhatsapp = assets && (getAsset(assets, 'cko7ntkiomww70c04nenfxybc')?.url)

	return <Head>
		<title>{headTitle}</title>
		<meta
			name='description'
			content={headDescription}
			key='description'
		/>
		{/* OG properties: title, image, etc., used for sharing on social media. https://ogp.me/ */}
		<meta property='og:title' content={headTitle} key='ogtitle' />
		<meta property='og:description' content={headDescription} key='ogdescription' />
		<meta property='og:site_name' content={headTitle} key='ogsitename' />
		<meta property='og:type' content='website' key='ogtype' />
		<meta property='og:url' content={currentHref} key='ogurl' />
		{ogImageWhatsapp && <meta property='og:image' content={ogImageWhatsapp} />}
		<meta property='og:image:width' content='305' />
		<meta property='og:image:height' content='305' />

		<link rel='icon' href='/favicon.png' />
		<link rel='alternate' hrefLang={locale} href={currentHref} />
		<link rel='alternate' hrefLang={counterLocale} href={counterHref} />
		{/* For manifest.json */}
		<link rel='manifest' href='/manifest.json' />
		<meta name='theme-color' content='#90cdf4' />
		{/* Sets logo in Apple smartphones */}
		<link rel='apple-touch-icon' href='/palette192.webp' />
		{/* Sets the color of url bar in Apple smartphones */}
		<meta name='apple-mobile-web-app-status-bar' content='#90cdf4' />
	</Head>
}

CustomHead.defaultProps = {
	// eslint-disable-next-line max-len
	assets: null,
}
