import React, { Fragment } from 'react'
import { Trans } from 'next-i18next'
import { useRouter } from 'next/router'

import FacebookLogo from '../elements/FacebookLogo'
import getWrittenOutDate from '../util/getWrittenOutDate'
import InternalLink from '../elements/InternalLink'

export default function Footer({ today, menu }) {
	const route = useRouter()
	const { menuHeadings } = menu
	const { locale } = route

	return <footer className='bg-primary small w-100'>
		<div className='d-flex justify-content-between align-items-center text-white flex-column flex-lg-row p-3'>
			<div className='text-center text-md-left'>
				<div>
					Met Potlood en Penseel - <Trans i18nKey='footer_last_change'>
						Laatste wijziging:
					</Trans>
					{' '}
					<span className='d-md-none'>{getWrittenOutDate(today, locale, true)}</span>
					<span className='d-none d-md-inline-block'>{getWrittenOutDate(today, locale)}</span>
				</div>
				<div className='mt-1'>
					<Trans i18nKey='footer_layout_title'>
						Opmaak:
					</Trans>{' '}
					<Trans i18nKey='footer_layout_description'>
						Marlein Rusch
					</Trans> |{' '}
					<Trans i18nKey='footer_management_title'>
						Beheer:
					</Trans>{' '}
					<Trans i18nKey='footer_management_description'>
						Jurgen Rusch en Marlein Rusch
					</Trans>{' '}
				</div>
			</div>
			<div className='d-flex align-items-center text-center mt-3 mt-lg-0'>
				<FacebookLogo dimension='25px' color='white' />
				{
					menuHeadings.map(({ id, title, slug }) => slug === 'sitemap'
						// (Internal)Link prepends locale, so just anchor tag here
						? <a key={id} href='/sitemap.xml' className='mx-2 text-white text-decoration-none fw-normal'>
							{title}
						</a>
						: <Fragment key={id}>
							<InternalLink className='mx-2 text-white text-decoration-none fw-normal' href={`/${slug}`}>
								{title}
							</InternalLink>|
						</Fragment>)
				}
			</div>
		</div>
	</footer>
}
