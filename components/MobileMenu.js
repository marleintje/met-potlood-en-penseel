import React from 'react'
import { useRouter } from 'next/router'

import BackDrop from '../elements/BackDrop'
import LocaleSwitch from '../elements/LocaleSwitch'

export default function MobileMenu({
	menuHeadings,
	pageUrls,
	menuIsOpen,
	setMenuIsOpen,
}) {
	const route = useRouter()
	const isOpen = menuIsOpen ? 'is-open' : ''
	
	const goToPage = (slug) => {
		route.push(slug, slug, { locale: route.locale ?? 'nl' })
		setMenuIsOpen(false)
	}

	return <>
		<div className={`d-lg-none bg-between-sections position-fixed mobile-header-menu ${isOpen}`}>
			<div className='pt-2'>
				{
					menuHeadings.map((menuHeading) => (<div
						className='mx-4 my-2'
						key={menuHeading.id}
					>
						<button
							className='btn btn-none p-0 h4 text-primary'
							type='button'
							onClick={() => goToPage(menuHeading.slug)}
						>
							{menuHeading.title}
						</button>
						{
							menuHeading.subHeaders
								// Otherwise the mobile menu is too long for the average mobile screen
								.filter(_ => !(menuHeading.slug === 'over-ons' || menuHeading.slug === 'about-us'))
								.map((subMenuItem, i) => <div
									key={subMenuItem.id}
									className='ps-3 text-start'
								>
									<button
										className='btn btn-none p-0 text-link'
										type='button'
										onClick={() => goToPage(`${menuHeading.slug}${i === 0 ? '' : (`#${subMenuItem.slug}`)}`)}
									>
										{subMenuItem.title}
									</button>
								</div>)
						}

					</div>))}
				<div className='mx-4 mt-4'>
					<LocaleSwitch pageUrls={pageUrls} setMenuIsOpen={setMenuIsOpen} />
				</div>
			</div>
		</div>
		<BackDrop onClick={() => setMenuIsOpen(false)} isOpen={isOpen} />
	</>
}
