import Container from 'react-bootstrap/Container'

import React from 'react'

const Text = ({text: { html }}) => <Container className='mt-2 mb-4 mb-lg-5'>
	<div dangerouslySetInnerHTML={{__html: html}} />
</Container>

export default Text