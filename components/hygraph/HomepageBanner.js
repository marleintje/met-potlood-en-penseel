import React from 'react'
import BannerAtelierLayover from '../BannerAtelierLayover'
import { useTranslation } from 'next-i18next'

export default function HomepageBanner({imageDesktop, imageMobile, imageTablet, titleColor}) {
	const { t } = useTranslation();
	// TODO-PAGES Sync translations, of hoe we dat hier ook doen, en slug zeker fixen voor EN
	const bannerButtonSlug = t('slug_homepage_banner', 'over-ons')

	return <>
		<div className='d-sm-none position-relative overflow-hidden'>
			<BannerAtelierLayover
				bannerButtonSlug={bannerButtonSlug}
				asset={imageMobile ?? imageDesktop}
				titleColor={titleColor?.hex}
			/>
		</div>
		<div className='d-none d-sm-block d-xl-none position-relative overflow-hidden'>
			<BannerAtelierLayover
				bannerButtonSlug={bannerButtonSlug}
				screenSize='medium'
				asset={imageTablet ?? imageDesktop}
				titleColor={titleColor?.hex}

			/>
		</div>
		<div className='d-none d-xl-block position-relative overflow-hidden'>
			<BannerAtelierLayover
				bannerButtonSlug={bannerButtonSlug}
				screenSize='large'
				asset={imageDesktop}
				titleColor={titleColor?.hex}
			/>
		</div>
	</>
}