import React from 'react'

const Spacer = ({ desktop, mobile }) => <div className={`py-${mobile} py-lg-${desktop}`} />

export default Spacer