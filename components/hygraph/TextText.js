import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'

import React from 'react'
import getBsFraction from '../../util/getBsFraction'

const TextImage = ({textLeft, textRight, fractionTextLeft}) => 
	<Container className='my-3 my-lg-4'>
		<Row>
			<Col
				className='mb-3 mb-lg-0'
				xs={12}
				md={getBsFraction(fractionTextLeft)}
			>
				<div dangerouslySetInnerHTML={{__html: textLeft.html}} />
			</Col>
			<Col
				xs={12}
				md={getBsFraction(fractionTextLeft, true)}
			>
				<div dangerouslySetInnerHTML={{__html: textRight.html}} />
			</Col>
		</Row>
	</Container>


export default TextImage
