import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'

import React from 'react'
import { Trans, useTranslation } from 'next-i18next'
import getBulkText from '../../util/findvalue/getBulkText'
import { useRouter } from 'next/router'
import getHtmlDiv from '../../util/getHtmlDiv'
import Image from 'next/image'
import getSrc from '../../util/getSrc'
import getWrittenOutDate from '../../util/getWrittenOutDate'
import InternalLink from '../../elements/InternalLink'

export default function LatestNewsSummary({
	isVideoLeft,
	videoCaption,
	video,
	bulkTexts,
	latestThreeAnnouncements,
}) {
	const route = useRouter()
	const { locale } = route
	const { t } = useTranslation()
	const bulkMoreNewsText = getBulkText(bulkTexts, 'homepage-more-news')

	return <Container>
		<Row className='my-5 my-lg-7'>
			<Col className='mt-3' md={{ order: isVideoLeft ? 3 : 1, span: 7 }}>
				<h2 id={locale === 'nl' ? 'laatste-nieuws' : 'latest-news'}>
					<Trans i18nKey='homepage_latest_news'>
						Laatste nieuws
					</Trans>
				</h2>
				
				<ul className='list-unstyled'>
					{
						latestThreeAnnouncements.map(announcement => <li key={announcement.id}>
							<span className='fw-bold'>
								{getWrittenOutDate(new Date(announcement.sortedDate), locale)}
							</span>{' '}
							-{' '}
							<span>{announcement.title} -</span>{' '}
							<InternalLink
								// TODO-PAGES voeg ook NL vertaling van slug toe ('actueel')
								href={`/${(t('slug_updates', 'updates'))}#${locale === 'nl' ? 'nieuws' : 'notice-board'}`}
							>
								<Trans i18nKey='read_more'>
									Lees meer
								</Trans>...
							</InternalLink>
						</li>)
					}
				</ul>
				{bulkMoreNewsText}
			</Col>
			<Col md={{ order: 2, span: 1 }} />
			<Col className='mt-3' md={{ order: isVideoLeft ? 1 : 3, span: 4 }}>
				{
					!!videoCaption && <div>{videoCaption}</div>
				}
				<div className='d-flex h-100 align-items-center'>
					<a
						aria-label={`[video thumbnail] - ${t(
							'homepage_movie_caption', 'Voor een filmimpressie, klik op het filmpje',
						)}`}
						className='position-relative'
						target='_blank'
						rel='noreferrer'
						href='https://youtu.be/voknxMQJNqE'
					>
						<Image
							blurDataURL={video.prefetchUrl}
							placeholder={video.prefetchUrl ? 'blur' : null}
							alt={`[video thumbnail] - ${t(
								'homepage_movie_caption', 'Voor een filmimpressie, klik op het filmpje',
							)}`}
							src={getSrc(video.url)}
							width={video.width}
							height={video.height}
						/>
						<div className='position-absolute play-button' />
					</a>
				</div>
			</Col>
		</Row>
	</Container>
}