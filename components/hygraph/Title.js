import React from 'react'
import slugify from '../../util/slugify'

export default function Title({
	title, depth, className = null
}) {
	switch (depth) {
		case 2:
			return <h2 id={slugify(title)} className={className ?? undefined}>{title}</h2>
		case 3:
			return <h3 id={slugify(title)} className={className ?? undefined}>{title}</h3>
		case 4:
			return <h4 className={className ?? undefined}>{title}</h4>
		case 5:
			return <h5 className={className ?? undefined}>{title}</h5>
		default:
			return <h2 className={className ?? undefined}>{title}</h2>
	}
}