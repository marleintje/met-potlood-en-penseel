import Card from 'react-bootstrap/Card'
import React from 'react'
import getSrc from '../../util/getSrc'

const ImageCard = ({text: { html }, image}) => <Card className='h-100'>
		<Card.Img alt='Information card' variant='top' src={getSrc(image.url)} />
		<Card.Body className='h-100 d-flex flex-column'>
			<div className='card-text' dangerouslySetInnerHTML={{__html: html}} />
		</Card.Body>
	</Card>

export default ImageCard
