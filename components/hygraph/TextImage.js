import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'

import React from 'react'
import CenteredColumnImage from '../../elements/CenteredColumnImage'
import getBsFraction from '../../util/getBsFraction'

const TextImage = ({text: { html }, textFraction, isImageLeft, image}) => 
	<Container className='my-3 my-lg-4'>
		<Row>
			<Col
				className='mb-3 mb-lg-0'
				xs={{span: 12, order: 1}}
				md={{span: getBsFraction(textFraction, true), order: isImageLeft ? 1 : 2}}
			>
				<CenteredColumnImage {...image} />
			</Col>
			<Col
				xs={{span: 12, order: 2}}
				md={{span: getBsFraction(textFraction), order: isImageLeft ? 2 : 1}}
			>
				<div dangerouslySetInnerHTML={{__html: html}} />
			</Col>
		</Row>
	</Container>


export default TextImage
