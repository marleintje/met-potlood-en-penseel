/* eslint-disable sort-keys */
import {
	Col, Container, Row,
} from 'react-bootstrap'
import React, { useContext } from 'react'
import { useRouter } from 'next/router'

import { Trans, useTranslation } from 'next-i18next'
import CustomHead from './CustomHead'
import ExhibitionItem from './ExhibitionItem'
import getItemsPerDay from '../util/getItemsPerDay'
import getYearlyExhibitionYears from '../util/getYearlyExhibitionYears'
import InternalLink from '../elements/InternalLink'
import Wrapper from './Wrapper'
import ZoomedImage from './ZoomedImage'
import ZoomImageContext from './ZoomImageContext'

const PageYearExhibition = ({
	alerts,
	assets,
	today,
	yearlyExhibitionItems,
	yearlyExhibitionItemsBulk,
	menus,
	pageUrls,
}) => {
	// TODO: SEO for this page. Can now be done with pages/API! See other projects.

	// Yearly exhibition items bulk was added later, to allow content editors to upload multiple paintings for one year at once.
	const flattenedBulkPaintings = yearlyExhibitionItemsBulk.flatMap(bulk => 
		bulk.paintings.map(painting => ({
			painting,
			year: bulk.year,
			title: bulk.title
		}))
	);

	const route = useRouter()
	const { t } = useTranslation()
	const { section, year } = route.query
	const { locale } = route
	const { zoomedImage, updateZoomedImage } = useContext(ZoomImageContext)

	const yearlyItemsPerDay = getItemsPerDay(yearlyExhibitionItems)

	const years = getYearlyExhibitionYears()
	const previousYear = Number(year) - 1
	const nextYear = Number(year) + 1

	const weekdayTranslations = {
		monday: t('monday', 'Maandag'),
		tuesday: t('tuesday', 'Dinsdag'),
		wednesday: t('wednesday', 'Woensdag'),
		thursday: t('thursday', 'Donderdag'),
		friday: t('friday', 'Vrijdag'),
		saturday: t('saturday', 'Zaterdag'),
		sunday: t('sunday', 'Zondag'),
	}

	const exhibitionYearNavigator = <div className='d-flex justify-content-between my-3'>
		{
			years.indexOf(previousYear) > -1
			&& <InternalLink prefetch={false} href={`/${section}/${previousYear}`}>
				&laquo; <Trans i18nKey='yearly_exhibition'>
					Jaarlijkse tentoonstelling
				</Trans> {previousYear}
			</InternalLink>
		}
		{
			years.indexOf(nextYear) > -1
			&& <InternalLink className='ms-auto' href={`/${section}/${nextYear}`}>
				<Trans i18nKey='yearly_exhibition'>
					Jaarlijkse tentoonstelling
				</Trans> {nextYear} &raquo;
			</InternalLink>
		}
	</div>

	return <>
		<CustomHead
			pageUrls={pageUrls}
			assets={assets}
			title={`${locale === 'nl' ? 'Jaarlijkse tentoonstelling' : 'Yearly exhibition'} | Met Potlood en Penseel.`}
		/>
		<Wrapper menus={menus} pageUrls={pageUrls} alerts={alerts} today={today}>
			<Container className='my-4 my-lg-5'>
				<ZoomedImage zoomedImage={zoomedImage} updateZoomedImage={updateZoomedImage} />
				<h1><Trans i18nKey='yearly_exhibition'>
					Jaarlijkse tentoonstelling
				</Trans> {year}</h1>
				<p>
					<Trans i18nKey='yearly_exhibition_explained'>
						{/* eslint-disable-next-line max-len */}
						Jaarlijks vindt er een expositie plaats bij Potlood en Penseel. Op deze pagina kunt u de foto&apos;s van alle werken terugvinden. Klik op een foto om deze te vergroten!
					</Trans>
				</p>
				{exhibitionYearNavigator}
				{
					!yearlyExhibitionItems?.length && !flattenedBulkPaintings?.length
						? <p><Trans i18nKey='yearly_exhibition_no_data'>
							Er zijn (nog) geen schilderijen beschikbaar voor het jaar
						</Trans> {year}</p>
						: <>
							{Object.keys(yearlyItemsPerDay)
								.map((weekday, i) => {
								const weekdayPaintings = yearlyItemsPerDay[weekday]
									.map((item, j) => <ExhibitionItem item={item} key={j} />)
								return (weekdayPaintings.length !== 0) && <Row key={i}>
									<Col xs={12}>
										<h2 className='mb-0 mt-4'>{weekdayTranslations[weekday]}</h2>
									</Col>
									{weekdayPaintings}
								</Row>
							})}
							{
								flattenedBulkPaintings.length !== 0 && <Row>
									{flattenedBulkPaintings.map((bulkPainting, j) => <ExhibitionItem item={bulkPainting} key={j} />)}
								</Row>
							}
						</>
				}
				
			</Container>
		</Wrapper>
	</>
}

export default PageYearExhibition
