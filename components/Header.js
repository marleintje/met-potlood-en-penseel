/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import {
	Nav, Navbar, NavDropdown,
} from 'react-bootstrap'
import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useRouter } from 'next/router'

import InternalLink from '../elements/InternalLink'
import LocaleSwitch from '../elements/LocaleSwitch'
import MobileMenu from './MobileMenu'

export default function Header({ menu: { menuHeadings }, pageUrls }) {
	const route = useRouter()
	const [menuIsOpen, setMenuIsOpen] = useState(false)
	const { locale } = route

	return <header>
		<Navbar className='shadow-sm fixed-top' bg='light' expand='lg'>
			<div className='container-fluid'>
				<div>
					<InternalLink>
						<div className='position-absolute opacity-0'>
							Back to homepage
						</div>
						<FontAwesomeIcon
							className='text-primary fa-xl'
							icon='palette'
						/>
					</InternalLink>
				</div>
				<div className='h3 header-title ms-3 mx-lg-4 flex-grow-1'>
					<InternalLink className='rakkas'>Met Potlood en Penseel</InternalLink>
				</div>
				<div className='d-lg-none'>
					<button
						aria-label='Toggle mobile menu'
						className='btn btn-none p-0'
						type='button'
						onClick={() => setMenuIsOpen(true)}
					>
						<FontAwesomeIcon className='fa-md' icon='bars' />
					</button>
				</div>
				<div className='d-none d-lg-block desktop-header-menu'>
					{/* TODO: Follow Navbar syntax as on react-bootstrap, and ultimately use <Link href="xx" passHref><Nav.Link>Home</Nav.link></Link> to use Next link */}
					<Navbar.Collapse id='basic-navbar-nav'>
						<Nav className='me-auto'>
							{
								menuHeadings.map((menuHeadingTopLevel) => {
									const { subHeaders } = menuHeadingTopLevel
									return subHeaders.length
										? <NavDropdown
											className='px-2 hover-link'
											key={menuHeadingTopLevel.id}
											title={menuHeadingTopLevel.title}
											id={menuHeadingTopLevel.id}
										>
											{
												subHeaders
													.map((subHeaders, i) =>
														<NavDropdown.Item
															key={i}
															href={
																`/${locale}/${menuHeadingTopLevel.slug}${i === 0 ? '' : (`#${subHeaders.slug}`)}`
															}
														>
															{subHeaders.title}
														</NavDropdown.Item>)
											}
											</NavDropdown>
										: <InternalLink className='text-headerlight p-2' href={`/${menuHeadingTopLevel.slug}`} key={i}>
											{menuHeadingTopLevel.title}
										</InternalLink>
								})
							}
						</Nav>
					</Navbar.Collapse>
				</div>
				<div className='d-none d-lg-block ms-3'>
					<LocaleSwitch pageUrls={pageUrls} />
				</div>
			</div>
		</Navbar>
		<div className='d-lg-none'>
			<MobileMenu
				pageUrls={pageUrls}
				menuHeadings={menuHeadings}
				menuIsOpen={menuIsOpen}
				setMenuIsOpen={setMenuIsOpen}
			/>
		</div>
	</header>
}
