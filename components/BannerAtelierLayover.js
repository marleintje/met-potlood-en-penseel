import React, { useEffect, useRef, useState } from 'react'
import { Button } from 'react-bootstrap'
import Image from 'next/image'
import { Trans } from 'next-i18next'
import { useRouter } from 'next/router'

import getSrc from '../util/getSrc'
import throttle from '../util/throttle'

export default function BannerAtelierLayover({
	bannerButtonSlug, screenSize, asset, titleColor = '#fff',
}) {
	const route = useRouter()
	const { locale } = route
	const {
		height, prefetchUrl, url, width,
	} = asset
	const [bannerTextPosition, setBannerTextPosition] = useState('start-position')
	const [bannerButtonPosition, setBannerButtonPosition] = useState('start-position')

	useEffect(() => {
		setTimeout(() => setBannerTextPosition(''), 200)
		setTimeout(() => setBannerButtonPosition(''), 600)
	}, [])

	const parallaxElement = useRef(null)

	useEffect(() => {
		const parallax = () => {
			if (!parallaxElement.current) return
			const scrolled = window.pageYOffset
			// You can adjust the 0.3 to change the speed
			const coordinates = `${scrolled * 0.3}px`
			parallaxElement.current.style.transform = `translateY(${coordinates})`
		}

		const handleScroll = throttle(parallax, 14)
		window.addEventListener('scroll', handleScroll)
		// Remove eventlistener on homepage leave
		return () => window.removeEventListener('scroll', handleScroll)
	})

	return <Button
		variant='none'
		onClick={() => route.push(bannerButtonSlug, bannerButtonSlug, { locale })}
		className='parallax p-0 w-100 h-100 border-none'
		ref={parallaxElement}
	>
		<Image
			blurDataURL={prefetchUrl}
			placeholder={prefetchUrl ? 'blur' : null}
			alt={`Homepage banner ${screenSize} screens`}
			src={getSrc(url)}
			height={height * 1.5}
			width={width * 1.5}
		/>
		<span
			className={
				`position-absolute homepage-banner-button btn btn-primary
					${screenSize !== 'small' ? 'btn-lg' : ''} ${bannerButtonPosition}`
			}
		>
			<Trans i18nKey='homepage_banner_button'>Bekijk ons atelier</Trans>
		</span>
		<h1 style={{color: titleColor}} className={`homepage-banner-title rakkas position-absolute ${bannerTextPosition}`}>
			<span className='d-block'>Met Potlood</span>
			<span className='d-block en-penseel'>en Penseel</span>
		</h1>
	</Button>
}

BannerAtelierLayover.defaultProps = {
	asset: {},
	screenSize: 'small',
}
