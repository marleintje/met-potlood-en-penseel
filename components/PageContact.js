/* eslint-disable max-len */
import { Col, Row } from 'react-bootstrap'
import React, { useEffect, useState } from 'react'

import ContactForm from './ContactForm'
import FacebookLogo from '../elements/FacebookLogo'
import getHtmlDiv from '../util/getHtmlDiv'
import Title from './hygraph/Title'
import { useTranslation } from 'next-i18next'

const PageContact = ({ bulkTexts }) => {
	const [mapUrl, setMapUrl] = useState('')
	const { t } = useTranslation()

	useEffect(() => {
		setMapUrl('//www.openstreetmap.org/export/embed.html?bbox=5.4307276010513315%2C51.44088788720685%2C5.438725948333741%2C51.4441479351796&layer=mapnik&marker=51.44251626846312%2C5.43472945690155')
	}, [])

	return <>
		<Row>
			<Col xs={12} lg={6}>
				<Title title={t('title_contact_page_left_column', 'Address')} depth={2}/>
				{getHtmlDiv(bulkTexts.find(({key}) => key === 'contact_wijkgebouw-adres')?.text?.html)}
				<FacebookLogo dimension='40px' color='blue' />
				<br />
				<a
					className='mt-4'
					aria-label='Open street maps'
					href={mapUrl}
					rel='noreferrer'
					target='_blank'
				>Open in OpenStreetMap</a>
				<div className='embed-responsive embed-responsive-4by3'>
					<iframe
						style={{ height: '250px' }}
						className='embed-responsive-item border-0 w-100'
						src={mapUrl}
						title='map'
					/>
				</div>
			</Col>
			<Col xs={12} lg={6} className='mt-5 mt-lg-0'>
				<Title title={t('title_contact_page_right_column', 'Get in touch with us')} depth={2}/>
				<ContactForm />
			</Col>
		</Row>
	</>
}

export default PageContact
