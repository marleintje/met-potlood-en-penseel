import { createContext } from 'react'

const ZoomImageContext = createContext()

export default ZoomImageContext
