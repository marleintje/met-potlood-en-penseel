import {
	Button, Col, Row,
} from 'react-bootstrap'
import React, { useContext } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Image from 'next/image'
import { useRouter } from 'next/router'

import getHtmlDiv from '../util/getHtmlDiv'
import getHygraphDomain from '../util/getHygraphDomain'
import getSrc from '../util/getSrc'
import getWrittenOutDate from '../util/getWrittenOutDate'
import ZoomedImage from './ZoomedImage'
import ZoomImageContext from './ZoomImageContext'

export default function Announcement({
	announcement,
}) {
	const route = useRouter()
	const { locale } = route
	const { zoomedImage, updateZoomedImage } = useContext(ZoomImageContext)
	const {
		title, sortedDate, strippedHtml, text: { raw },
	} = announcement
	// Get images from text.raw property instead of text.html, so we can loop over them and nicely display
	const rawImages = raw.children.filter(rawTextPiece => rawTextPiece.type === 'image')

	return <>
		<ZoomedImage zoomedImage={zoomedImage} updateZoomedImage={updateZoomedImage} />
		<div className='card my-4 shadow-sm p-3 p-lg-4 pb-1 pb-lg-1'>
			<div className='d-flex align-items-center mb-1'>
				<div className='mt-2 me-3'>
					<FontAwesomeIcon
						className='fa-xl text-primary'
						icon='thumbtack'
					/>
				</div>
				<div className='w-100'>
					<h3 className='text-primary mb-0'>{title}</h3>
				</div>
			</div>
			<div className='mt-3'>
				{getHtmlDiv(strippedHtml)}
				<p className='text-link fst-italic'>{getWrittenOutDate(new Date(sortedDate), locale)}</p>
			</div>
			{
				!!(rawImages && rawImages.length) && <Row>
					{rawImages.map((rawImage, i) => {
						const { handle, title, height, width } = rawImage
						const prefetchWidth = Math.floor((width * 10) / height)
						const displayWidth = Math.floor((width * 200) / height)
					
						return <Col className='p-2' key={i} xs={6} sm={4} md={3}>
							<Button
								className='w-100 p-1 border next-image-thumbnail'
								onClick={() => updateZoomedImage(rawImage)}
								variant='none'
							>
								<Image
									placeholder='blur'
									blurDataURL={`${getHygraphDomain()}resize=fit:crop,height:10,width:${prefetchWidth}/${handle}`}
									alt={title}
									src={getSrc(`${getHygraphDomain()}resize=fit:crop,height:200,width:${displayWidth}/${handle}`)}
									layout='responsive'
									height={200 * 1.5}
									width={displayWidth * 1.5}
								/>
							</Button>
						</Col>})}
					</Row>
				}
		</div>
	
	</>
}
