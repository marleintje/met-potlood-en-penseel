import { Alert, Button } from 'react-bootstrap'
import React, { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import localForage from 'localforage'

import getHtmlDiv from '../util/getHtmlDiv'

const AlertBanner = ({ alert }) => {
	const {
		id, endDate, text, bootstrapColor,
	} = alert

	const [isVisible, setIsVisible] = useState(false)

	const now = new Date()
	const alertEndDate = new Date(endDate)
	const isPastEndDate = (!endDate) ? false : (now - alertEndDate) > 1
	const alertsKey = 'alerts'

	const disableAlert = async () => {
		const closedAlerts = await localForage.getItem(alertsKey) || []
		if (closedAlerts.indexOf(id) === -1) closedAlerts.push(id)
		setIsVisible(false)
		localForage.setItem(alertsKey, closedAlerts)
	}

	useEffect(() => {
		async function updateHomepageAlerts() {
			const closedAlerts = await localForage.getItem(alertsKey) || []
			if (closedAlerts.indexOf(id) === -1)
				setIsVisible(true)
		}
		updateHomepageAlerts()
	}, [])

	if (!isVisible || isPastEndDate) return null

	return <Alert
		variant={bootstrapColor}
		className={`border-${bootstrapColor} my-1 pt-3 pb-0 mb-0 overflow-hidden`}
	>
		<div className='text-center'>
			{getHtmlDiv(text.html)}
		</div>
		<Button
			aria-label='Close alert'
			variant='none'
			className='position-absolute top-0 right-0'
			onClick={disableAlert}
		>
			<FontAwesomeIcon
				className={`fa-sm text-${bootstrapColor}`}
				icon='times'
			/>
		</Button>
	</Alert>
}

AlertBanner.defaultProps = {
	bootstrapColor: 'info',
}

export default AlertBanner
