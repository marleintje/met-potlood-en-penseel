import { Button, Card, Col } from 'react-bootstrap'
import React, { useContext } from 'react'
import Image from 'next/image'

import getHygraphDomain from '../util/getHygraphDomain'
import getSrc from '../util/getSrc'
import ZoomImageContext from './ZoomImageContext'

export default function ExhibitionItem({
	item,
}) {
	const {
		artist, details, format, painting, title,
	} = item
	const resizeParameters = 'resize=fit:crop,height:200,width:200/'

	const { updateZoomedImage } = useContext(ZoomImageContext)

	return <Col className='mt-4' xs={6} md={4} lg={3}>
		<Card className='h-100'>
			<Button
				onClick={() => updateZoomedImage(painting)}
				variant='none'
				className='w-100 p-0'
			>
				<Image
					blurDataURL={painting?.prefetchUrl}
					placeholder={painting?.prefetchUrl ? 'blur' : null}
					alt={title}
					width={300}
					height={300}
					src={getSrc(`${getHygraphDomain()}${resizeParameters}${painting?.handle}`)}
				/>
			</Button>
			{
				(artist || details || format || title) && (
					<Card.Body className='h-100 d-flex flex-column p-2 pt-3 p-md-3'>
						{
							title && <Card.Title className='mb-0'>{title}</Card.Title>
						}
						{
							(artist || details || format) && <Card.Text>
								{artist && <span className='d-block'><em>{artist}</em></span>}
								{details && <span className='d-block mt-3'>{details}</span>}
								{format && <span className='small'><em>{format}</em></span>}
							</Card.Text>
						}
					</Card.Body>
				)
			}
		</Card>
	</Col>
}
