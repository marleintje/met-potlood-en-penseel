import React from 'react'

import AlertBanner from './AlertBanner'
import Footer from './Footer'
import Header from './Header'

const Wrapper = ({
	alerts, children, today, menus, pageUrls
}) => {
	const footerMenu = menus.find(({key}) => key === 'footer');
	const headerMenu = menus.find(({key}) => key === 'header');

	return <>
		<Header pageUrls={pageUrls} menu={headerMenu} alerts={alerts} />
		<main>
			{Array.isArray(alerts) && alerts.map(
				alert => <AlertBanner key={alert.id} alert={alert} />,
			)}
			{children}
		</main>
		<Footer menu={footerMenu} today={today} />
	</>
}

export default Wrapper
