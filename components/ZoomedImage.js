import React, { useEffect, useState } from 'react'
import Image from 'next/image'

import BackDrop from '../elements/BackDrop'

// This image is visible (with backdrop), when clicking on an prikbord/archive image.
export default function ZoomedImage({ zoomedImage, updateZoomedImage }) {
	if (!zoomedImage) return null
	const zoomedImageIsOpen = (zoomedImage.src || zoomedImage.url) ? 'is-open' : ''

	const {
		fileName, // FileName for Exhibition items
		title, // Title for Announcments
		width,
		height,
		src, // Src for Announcements
		prefetchUrl, // For blurry placeholder
		url, // Url for Exhibition items
	} = zoomedImage

	const [viewportWidth, setViewportWidth] = useState(null)

	useEffect(() => {
		setViewportWidth(window.innerWidth)
	}, [])

	const maxZoomWidth = 0.9 * viewportWidth

	// If image width exceeds max allowed width, then set it to max allowed width
	const zoomedWidth = width > maxZoomWidth ? maxZoomWidth : width
	// If image width exceeds max allowed width, then adjust the height according to original height/width ratio
	const zoomedHeight = width > maxZoomWidth
		? (height * maxZoomWidth) / width
		: height

	return <>
		<div className={`zoomed-image ${zoomedImageIsOpen}`}>
			<Image
				blurDataURL={prefetchUrl}
				placeholder={prefetchUrl ? 'blur' : null}
				alt={`${title || fileName} zoomed in`}
				src={src || url}
				width={zoomedWidth}
				height={zoomedHeight}
			/>
		</div>
		<BackDrop onClick={() => updateZoomedImage('')} isOpen={zoomedImageIsOpen} />
	</>
}
