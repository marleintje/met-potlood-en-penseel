import React, { useContext } from 'react'
import { Trans, useTranslation } from 'next-i18next'
import { Container } from 'react-bootstrap'
import { useRouter } from 'next/router'

import Announcement from './Announcement'
import CustomHead from './CustomHead'
import getArchiveYears from '../util/getArchiveYears'
import InternalLink from '../elements/InternalLink'
import Wrapper from './Wrapper'
import ZoomedImage from './ZoomedImage'
import ZoomImageContext from './ZoomImageContext'

const PageYearArchive = ({
	alerts,
	assets,
	announcements,
	today,
	pageUrls,
	menus
}) => {
	const route = useRouter()
	const { t } = useTranslation()
	const { section, year } = route.query
	const { locale } = route

	const { zoomedImage, updateZoomedImage } = useContext(ZoomImageContext)

	const archiveYearAnnouncements = Array.isArray(announcements) && announcements.filter(
		announcement => (new Date(announcement.sortedDate)).getFullYear() === Number(year),
	)

	const backToNews = <InternalLink href={`/${locale === 'nl' ? 'actueel' : 'updates'}#${t('archive', 'Archief').toLowerCase()}`}>
		&laquo; <Trans i18nKey='archive_back_to_updates'>
			Terug naar het prikbord
		</Trans>
	</InternalLink>

	const years = getArchiveYears()
	const previousYear = Number(year) - 1
	const nextYear = Number(year) + 1

	const archiveYearNavigator = <div className='d-flex justify-content-between my-3'>
		{
			years.indexOf(previousYear) > -1
			&& <InternalLink prefetch={false} href={`/${section}/${previousYear}`}>
				&laquo; <Trans i18nKey='archive'>
					Archief
				</Trans>{' '}
				{previousYear}
			</InternalLink>
		}
		{
			years.indexOf(nextYear) > -1
			&& <InternalLink prefetch={false} href={`/${section}/${nextYear}`}>
				<Trans i18nKey='archive'>
					Archief
				</Trans>{' '}
				{nextYear} &raquo;
			</InternalLink>
		}
	</div>

	return <>
		<CustomHead pageUrls={pageUrls} assets={assets} title={`${locale === 'nl' ? 'Archief' : 'Archive'} | Met Potlood en Penseel.`} />
		<Wrapper pageUrls={pageUrls} menus={menus} alerts={alerts} today={today}>
			<ZoomedImage zoomedImage={zoomedImage} updateZoomedImage={updateZoomedImage} />
			<Container className='my-4 my-lg-5'>
				<h1 className='mb-1'>
					<Trans i18nKey='archive'>
						Archief
					</Trans>
				</h1>
				<div className='mb-5'>
					{backToNews}
				</div>
				{archiveYearNavigator}
				<h2>{year}</h2>
				{
					archiveYearAnnouncements.length
						? <>
							{archiveYearAnnouncements.map(announcement =>
								<Announcement
									key={announcement.id}
									announcement={announcement}
								/>)}
							{backToNews}
							{archiveYearNavigator}
						</>
						: <div className='mt-4'>
							<Trans i18nKey='archive_no_announcements_this_year'>
								Er zijn nog geen gearchiveerde nieuwtjes voor dit jaar. Zie het archief voor berichten van voorgaande jaren.
							</Trans>
						</div>
				}
			</Container>
		</Wrapper>
	</>
}

export default PageYearArchive
