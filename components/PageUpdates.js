import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import React from 'react'
import { Trans, useTranslation } from 'next-i18next'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useRouter } from 'next/router'

import Announcement from './Announcement'
import getArchiveYears from '../util/getArchiveYears'
import getYearlyExhibitionYears from '../util/getYearlyExhibitionYears'
import InternalLink from '../elements/InternalLink'
import Title from './hygraph/Title'
import getBulkText from '../util/findvalue/getBulkText'

const PageUpdates = ({
	assets,
	announcements,
	bulkTexts,
}) => {
	const route = useRouter()
	const { t } = useTranslation()
	const { locale } = route

	const thisYear = new Date().getFullYear()
	const thisYearsAccouncements = Array.isArray(announcements) && announcements.filter(
		announcement => ((new Date(announcement.sortedDate)).getFullYear() === thisYear),
	)
	const archiveYears = getArchiveYears()
	const yearlyExhibitionYears = getYearlyExhibitionYears()

	const lastYear = new Date().getFullYear() - 1

	return <>
		<Row>
			<Col xs={12}>
				<Title className='mb-0' title={t('title_updates_page_notice_board', 'Notice board')} depth={2}/>
				<i className='small'>
					<Trans i18nKey='announcements_none_this_year_see_next_year'>
						Zoek je een nieuwtje van vorig jaar? Bekijk deze dan <InternalLink href={`/${locale === 'nl' ? `archief/${lastYear}` : `archive/${lastYear}`}`} prefetch={false}>hier</InternalLink>.
					</Trans>
				</i>
				{
					thisYearsAccouncements.length === 0
						? <p>
							<Trans i18nKey='announcements_none_this_year'>
								Er zijn geen nieuwtjes geplaatst voor het jaar
							</Trans> {thisYear}.{' '}
							</p>
						: thisYearsAccouncements.map((announcement, i) => <Announcement key={i} announcement={announcement} />)
				}
		
			</Col>
		</Row>
		<Title className='mt-3 mt-lg-5' title={t('title_updates_page_archive', 'Archive')} depth={2}/>
		<Title className='mt-3' title={t('title_updates_page_archive_subtitle', 'News messages from previous years')} depth={3}/>
		<div id={locale === 'nl' ? 'archief' : 'archive'}>
			{archiveYears.map(year =>
				<InternalLink
					className='btn btn-primary bg-link border border-link text-decoration-none btn-lg me-2 mt-2'
					key={year}
					href={`/${locale === 'nl' ? 'archief' : 'archive'}/${year}`}
					// https://web.dev/route-prefetching-in-nextjs/#avoid-unnecessary-prefetching
					prefetch={false}
				>
					{year}
				</InternalLink>)}
		</div>
		<div id={locale === 'nl' ? 'jaarlijkse-tentoonstelling' : 'yearly-exhibition'}>
			<Title className='mt-4 mt-lg-6' title={t('title_updates_page_yearly_exhibition', 'Yearly Exhibition')} depth={2}/>
			{getBulkText(bulkTexts, 'actueel_jaarlijkse-tentoonstelling')}
			{yearlyExhibitionYears && yearlyExhibitionYears.map(year =>
				<InternalLink
					className='btn btn-primary bg-link border border-link text-decoration-none btn-lg me-2 mt-2'
					key={year}
					href={`/${locale === 'nl' ? 'jaarlijkse-tentoonstelling' : 'yearly-exhibition'}/${year}`}
					// https://web.dev/route-prefetching-in-nextjs/#avoid-unnecessary-prefetching
					prefetch={false}
				>
					{year}
				</InternalLink>)}
		</div>
		<Title className='mt-4 mt-lg-6' title={t('title_updates_page_yearly_exhibition_subtitle', 'The magazine "De Puntenslijper"')} depth={3}/>
		{getBulkText(bulkTexts, 'actueel_puntenslijper')}
		{assets
			.filter(asset => asset.fileName.indexOf('puntenslijper-') > -1)
			.sort((a, b) => ((a.fileName < b.fileName) ? 1 : -1))
			.map((pdf, i) => {
				const title = (pdf.fileName).split('puntenslijper-')[1].split('.pdf')[0]
				return <a key={i} href={pdf.url} rel='noreferrer' target='_blank'>
					<span className='d-flex align-items-center my-2'>
						<FontAwesomeIcon icon='file-pdf' className='fa-sm text-primary me-2' />
						{title}
					</span>
				</a>
			})}
	</>
}

export default PageUpdates
