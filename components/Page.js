import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import CustomHead from '../components/CustomHead'
import Wrapper from '../components/Wrapper'
import HomepageBanner from '../components/hygraph/HomepageBanner'
import LatestNewsSummary from '../components/hygraph/LatestNewsSummary'
import Spacer from '../components/hygraph/Spacer'
import Text from '../components/hygraph/Text'
import TextText from '../components/hygraph/TextText'
import TextImage from '../components/hygraph/TextImage'
import Title from '../components/hygraph/Title'
import ImageCard from '../components/hygraph/ImageCard'

const Page = ({ page, alerts, today, announcements, bulkTexts, assets, menus, pageUrls}) => {
	const { title, seoTitle, seoDescription, components, url } = page
	const isHomePage = url === 'home'
	const latestThreeAnnouncements = Array.isArray(announcements) && announcements.slice(0, 5)

	return <Wrapper menus={menus} pageUrls={pageUrls} alerts={alerts} today={today}>
		{/* TODO PAGES bring zoomedimage back to live... why was it commented here? Is that OK? */}
		{/* <ZoomedImage zoomedImage={zoomedImage} updateZoomedImage={updateZoomedImage} /> */}
		<div className={`hygraph-components my-4 my-lg-5 ${isHomePage ? 'mt-0 mt-lg-0': ''}`}>
			{
				!isHomePage && <Container>
					<h1>{title}</h1>
				</Container>
			}
			<CustomHead
				pageUrls={pageUrls}
				assets={assets}
				description={seoDescription}
				title={seoTitle}
			/>
			{components?.map(component => {
				switch (component.__typename) {
					case 'HomepageBanner':
						return <HomepageBanner key={component.id} {...component} />
					case 'LatestNewsSummary':
						return <LatestNewsSummary key={component.id}
							bulkTexts={bulkTexts}
							latestThreeAnnouncements={latestThreeAnnouncements}
							{...component}
						/>
					case 'Spacer':
						return <Spacer key={component.id} {...component} />
					case 'Text':
						return <Text key={component.id} {...component} />
					case 'TextImage':
						return <TextImage key={component.id} {...component} />
					case 'TextText':
						return <TextText key={component.id} {...component} />
					case 'InformationCards':
						return <Container key={component.id}>
							<Row>
								{component.cards?.map(card => <Col key={card.id} className='mb-4 mb-lg-0' xs={12} lg={Math.floor(12 / component.cards.length)}>
									<ImageCard key={card.id} {...card} />
								</Col>)}
							</Row>
						</Container>
					case 'Title':
						return <Container key={component.id}>
							<Title {...component} />
						</Container>
					default:
						return null
				}
			})}
		</div>
	</Wrapper>
}
export default Page
