import { GraphQLClient } from 'graphql-request'

let hygraphClient = new GraphQLClient(process.env.HYGRAPH_ENDPOINT, {
	headers: {
		authorization: `Bearer ${process.env.HYGRAPH_CONTENT_API_TOKEN}`,
	},
})

const request = async qry => {
	const data = await hygraphClient.request(qry)
	return data
}

export default {
	request,
}
